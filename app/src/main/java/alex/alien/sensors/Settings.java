package alex.alien.sensors;


import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.os.Parcel;
import android.os.Parcelable;

import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.DataPointInterface;

public class Settings extends BaseObservable implements Parcelable {
    private final static int MAX_POINTS = 13;

    public static final int MAX_BRIGHTNESS = 255;
    public static final int MIN_BRIGHTNESS = 20;
    public static final int BOUNDARY_PERCENT = 35;
    public static final int BRIGHTNESS_DELTA_LOW = 25;
    public static final int BRIGHTNESS_DELTA_HIGH = 30;

    public static final int MIN_STEP_DELTA_LOW_BRIGHTNESS = 4;
    public static final int MIN_STEP_DELTA_HIGH_BRIGHTNESS = 10;
    public static final int STEPS_COUNT = 4;
    public static final int STEP_DURATION = 220;

    public static final int SENSOR_EVENT_DELAY = 1000000; // 1 sec
    public static final int BRIGHTNESS_CHANGE_DELAY = 2500; // 2.5 sec

    public static final String PREFERENCES_NAME = "AUTOBRIGHTNESS";
    public static final boolean COLLECT_SENSOR_VALUES = false;

    private int sensorEventDelay;
    private int brightnessChangeDelay;
    private int brightnessSteps;
    private int stepDuration;
    private int minStepDeltaLowBrightness;
    private int minStepDeltaHighBrightness;
    private int boundaryPercent;
    private int brightnessDeltaLow;
    private int brightnessDeltaHigh;
    private boolean collectSensorValues;
    private DataPointInterface[] dataPoints;

    private Settings() {
        setRealSensorEventDelay(SENSOR_EVENT_DELAY);
        setRealBrightnessChangeDelay(BRIGHTNESS_CHANGE_DELAY);
        setRealBrightnessSteps(STEPS_COUNT);
        setRealStepDuration(STEP_DURATION);
        setRealMinStepDeltaLowBrightness(MIN_STEP_DELTA_LOW_BRIGHTNESS);
        setRealMinStepDeltaHighBrightness(MIN_STEP_DELTA_HIGH_BRIGHTNESS);
        setRealBoundaryPercent(BOUNDARY_PERCENT);
        setRealBrightnessDeltaLow(BRIGHTNESS_DELTA_LOW);
        setRealBrightnessDeltaHigh(BRIGHTNESS_DELTA_HIGH);
        setRealCollectSensorValues(COLLECT_SENSOR_VALUES);
    }

    public static DataPointInterface[] getDefaultBrightnessPoints(Context context) {
        return ResourceManager.readDefaultBrightnessPointsFromResource(context);
    }

    public static DataPointInterface[] getLinearDataPoints(float maxSensorRange) {
        if (maxSensorRange <= 0) {
            return new DataPoint[0];
        }
        DataPointInterface[] points = new DataPoint[MAX_POINTS];
        float step = maxSensorRange / MAX_POINTS;
        points[0] = new DataPoint(0, 0);
        points[MAX_POINTS - 1] = new DataPoint(maxSensorRange, 100);
        for (int i = 1; i < MAX_POINTS - 1; i++) {
            float value = i * step;
            float percent = Math.round(value * 100 / maxSensorRange);
            points[i] = new DataPoint(value, percent);
        }
        return points;
    }

    public static Settings createInstance(Context context) {
        Settings settings = new Settings();
        settings.dataPoints = getDefaultBrightnessPoints(context);
        return settings;
    }

    public DataPointInterface[] getDataPoints() {
        return dataPoints;
    }

    public void setDataPoints(DataPointInterface[] points) {
        dataPoints = points;
    }

    @Bindable
    public int getSensorEventDelay() {
        return sensorEventDelay;
    }

    @Bindable
    public int getRealSensorEventDelay() { return (sensorEventDelay + 1) * 500000; }

    public void setSensorEventDelay(int sensorEventDelay) {
        this.sensorEventDelay = sensorEventDelay;
        notifyPropertyChanged(BR.sensorEventDelay);
        notifyPropertyChanged(BR.realSensorEventDelay);
    }

    public void setRealSensorEventDelay(int sensorEventDelay) {
        this.sensorEventDelay = sensorEventDelay / 500000 - 1;
        notifyPropertyChanged(BR.sensorEventDelay);
        notifyPropertyChanged(BR.realSensorEventDelay);
    }

    @Bindable
    public int getBrightnessChangeDelay() {
        return brightnessChangeDelay;
    }

    @Bindable
    public int getRealBrightnessChangeDelay() {
        return (brightnessChangeDelay + 1) * 500;
    }

    public void setBrightnessChangeDelay(int brightnessChangeDelay) {
        this.brightnessChangeDelay = brightnessChangeDelay;
        notifyPropertyChanged(BR.brightnessChangeDelay);
        notifyPropertyChanged(BR.realBrightnessChangeDelay);
    }

    public void setRealBrightnessChangeDelay(int brightnessChangeDelay) {
        this.brightnessChangeDelay = brightnessChangeDelay / 500 - 1;
        notifyPropertyChanged(BR.brightnessChangeDelay);
        notifyPropertyChanged(BR.realBrightnessChangeDelay);
    }

    @Bindable
    public int getBrightnessSteps() {
        return brightnessSteps;
    }

    @Bindable
    public int getRealBrightnessSteps() {
        return brightnessSteps + 1;
    }

    public void setBrightnessSteps(int brightnessSteps) {
        this.brightnessSteps = brightnessSteps;
        notifyPropertyChanged(BR.brightnessSteps);
        notifyPropertyChanged(BR.realBrightnessSteps);
    }

    public void setRealBrightnessSteps(int brightnessSteps) {
        this.brightnessSteps = brightnessSteps - 1;
        notifyPropertyChanged(BR.brightnessSteps);
        notifyPropertyChanged(BR.realBrightnessSteps);
    }

    @Bindable
    public int getRealStepDuration() {
        return (stepDuration + 1) * 200;
    }

    @Bindable
    public int getStepDuration() {
        return stepDuration;
    }

    public void setStepDuration(int stepDuration) {
        this.stepDuration = stepDuration;
        notifyPropertyChanged(BR.stepDuration);
        notifyPropertyChanged(BR.realStepDuration);
    }

    public void setRealStepDuration(int stepDuration) {
        this.stepDuration = stepDuration / 200 - 1;
        notifyPropertyChanged(BR.stepDuration);
        notifyPropertyChanged(BR.realStepDuration);
    }

    @Bindable
    public int getMinStepDeltaLowBrightness() {
        return minStepDeltaLowBrightness;
    }

    @Bindable
    public int getRealMinStepDeltaLowBrightness() {
        return (minStepDeltaLowBrightness + 1) * 2;
    }

    public void setMinStepDeltaLowBrightness(int minStepDeltaLowBrightness) {
        this.minStepDeltaLowBrightness = minStepDeltaLowBrightness;
        notifyPropertyChanged(BR.minStepDeltaLowBrightness);
        notifyPropertyChanged(BR.realMinStepDeltaLowBrightness);
    }

    public void setRealMinStepDeltaLowBrightness(int minStepDeltaLowBrightness) {
        this.minStepDeltaLowBrightness = minStepDeltaLowBrightness / 2 - 1;
        notifyPropertyChanged(BR.minStepDeltaLowBrightness);
        notifyPropertyChanged(BR.realMinStepDeltaLowBrightness);
    }

    @Bindable
    public int getMinStepDeltaHighBrightness() {
        return minStepDeltaHighBrightness;
    }

    @Bindable
    public int getRealMinStepDeltaHighBrightness() {
        return (minStepDeltaHighBrightness + 1) * 2;
    }

    public void setMinStepDeltaHighBrightness(int minStepDeltaHighBrightness) {
        this.minStepDeltaHighBrightness = minStepDeltaHighBrightness;
        notifyPropertyChanged(BR.minStepDeltaHighBrightness);
        notifyPropertyChanged(BR.realMinStepDeltaHighBrightness);
    }

    public void setRealMinStepDeltaHighBrightness(int minStepDeltaHighBrightness) {
        this.minStepDeltaHighBrightness = minStepDeltaHighBrightness / 2 - 1;
        notifyPropertyChanged(BR.minStepDeltaHighBrightness);
        notifyPropertyChanged(BR.realMinStepDeltaHighBrightness);
    }

    @Bindable
    public int getBoundaryPercent() {
        return boundaryPercent;
    }

    @Bindable
    public int getRealBoundaryPercent() {
        return (boundaryPercent + 1) * 5;
    }

    public void setBoundaryPercent(int boundaryPercent) {
        this.boundaryPercent = boundaryPercent;
        notifyPropertyChanged(BR.boundaryPercent);
        notifyPropertyChanged(BR.realBoundaryPercent);
    }

    public void setRealBoundaryPercent(int boundaryPercent) {
        this.boundaryPercent = boundaryPercent / 5 - 1;
        notifyPropertyChanged(BR.boundaryPercent);
        notifyPropertyChanged(BR.realBoundaryPercent);
    }

    @Bindable
    public int getBrightnessDeltaLow() {
        return brightnessDeltaLow;
    }

    @Bindable
    public int getRealBrightnessDeltaLow() {
        return (brightnessDeltaLow + 1) * 5;
    }

    public void setBrightnessDeltaLow(int brightnessDeltaLow) {
        this.brightnessDeltaLow = brightnessDeltaLow;
        notifyPropertyChanged(BR.brightnessDeltaLow);
        notifyPropertyChanged(BR.realBrightnessDeltaLow);
    }

    public void setRealBrightnessDeltaLow(int brightnessDeltaLow) {
        this.brightnessDeltaLow = brightnessDeltaLow / 5 - 1;
        notifyPropertyChanged(BR.brightnessDeltaLow);
        notifyPropertyChanged(BR.realBrightnessDeltaLow);
    }

    @Bindable
    public int getBrightnessDeltaHigh() {
        return brightnessDeltaHigh;
    }

    @Bindable
    public int getRealBrightnessDeltaHigh() {
        return (brightnessDeltaHigh + 1) * 5;
    }

    public void setBrightnessDeltaHigh(int brightnessDeltaHigh) {
        this.brightnessDeltaHigh = brightnessDeltaHigh;
        notifyPropertyChanged(BR.brightnessDeltaHigh);
        notifyPropertyChanged(BR.realBrightnessDeltaHigh);
    }

    public void setRealBrightnessDeltaHigh(int brightnessDeltaHigh) {
        this.brightnessDeltaHigh = brightnessDeltaHigh / 5 - 1;
        notifyPropertyChanged(BR.brightnessDeltaHigh);
        notifyPropertyChanged(BR.realBrightnessDeltaHigh);
    }

    @Bindable
    public boolean getCollectSensorValues() { return collectSensorValues; }

    @Bindable
    public boolean getRealCollectSensorValues() { return collectSensorValues; }

    public void setCollectSensorValues(boolean value) {
        this.collectSensorValues = value;
        notifyPropertyChanged(BR.collectSensorValues);
        notifyPropertyChanged(BR.realCollectSensorValues);
    }

    public void setRealCollectSensorValues(boolean value) {
        this.collectSensorValues = value;
        notifyPropertyChanged(BR.collectSensorValues);
        notifyPropertyChanged(BR.realCollectSensorValues);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(sensorEventDelay);
        parcel.writeInt(brightnessChangeDelay);
        parcel.writeInt(brightnessSteps);
        parcel.writeInt(stepDuration);
        parcel.writeInt(minStepDeltaLowBrightness);
        parcel.writeInt(minStepDeltaHighBrightness);
        parcel.writeInt(boundaryPercent);
        parcel.writeInt(brightnessDeltaLow);
        parcel.writeInt(brightnessDeltaHigh);
        parcel.writeInt(collectSensorValues ? 1 : 0);
        parcel.writeSerializable(dataPoints);
    }

    public static final Parcelable.Creator<Settings> CREATOR = new Parcelable.Creator<Settings>() {
        @Override
        public Settings createFromParcel(Parcel source) {
            int sensorEventDelay = source.readInt();
            int brightnessChangeDelay = source.readInt();
            int brightnessSteps = source.readInt();
            int stepDuration = source.readInt();
            int minStepDeltaLowBrightness = source.readInt();
            int minStepDeltaHighBrightness = source.readInt();
            int boundaryPercent = source.readInt();
            int brightnessDeltaLow = source.readInt();
            int brightnessDeltaHigh = source.readInt();
            boolean collectSensorValues = source.readInt() != 0;
            DataPointInterface[] points = (DataPointInterface[]) source.readSerializable();

            Settings settings = new Settings();
            settings.sensorEventDelay = sensorEventDelay;
            settings.brightnessChangeDelay = brightnessChangeDelay;
            settings.brightnessSteps = brightnessSteps;
            settings.stepDuration = stepDuration;
            settings.minStepDeltaLowBrightness = minStepDeltaLowBrightness;
            settings.minStepDeltaHighBrightness = minStepDeltaHighBrightness;
            settings.boundaryPercent = boundaryPercent;
            settings.brightnessDeltaLow = brightnessDeltaLow;
            settings.brightnessDeltaHigh = brightnessDeltaHigh;
            settings.collectSensorValues = collectSensorValues;
            settings.dataPoints = points;

            return settings;
        }

        @Override
        public Settings[] newArray(int size) {
            return new Settings[0];
        }
    };
}
