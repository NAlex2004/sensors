package alex.alien.sensors;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.DataPointInterface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class ResourceManager {
    public static final String DEFAULT_KEYS_ARRAY_NAME = "brightness_keys";
    public static final String DEFAULT_VALUES_ARRAY_NAME = "brightness_values";

    public static DataPointInterface[] readDefaultBrightnessPointsFromResource(Context context) {
        return readBrightnessPointsFromResource(context, DEFAULT_KEYS_ARRAY_NAME, DEFAULT_VALUES_ARRAY_NAME);
    }

    public static DataPointInterface[] readBrightnessPointsFromResource(Context context, String keysArrayName, String valuesArrayName) {
        Resources resources = context.getResources();
        String packageName = context.getPackageName();
        int keysId = resources.getIdentifier(keysArrayName, "array", packageName);
        int valuesId = resources.getIdentifier(valuesArrayName, "array", packageName);

        int[] keys = resources.getIntArray(keysId);
        int[] values = resources.getIntArray(valuesId);

        int length = Math.min(keys.length, values.length);
        DataPoint[] points = new DataPoint[length];

        for (int i = 0; i < length; i++) {
            points[i] = new DataPoint(keys[i], values[i]);
        }
        Arrays.sort(points, new Comparator<DataPoint>() {
            @Override
            public int compare(DataPoint p1, DataPoint p2) {
                return Double.compare(p1.getX(), p2.getX());
            }
        });
        return points;
    }

    public static String dataPointsToString(DataPointInterface[] points) {
        StringBuilder builder = new StringBuilder();
        for (DataPointInterface point : points) {
            builder.append(String.valueOf(point.getX()))
                    .append(":")
                    .append(String.valueOf(point.getY()))
                    .append(";");
        }
        if (builder.length() > 0) {
            builder.setLength(builder.length() - 1);
        }
        return builder.toString();
    }

    public static DataPointInterface[] stringToDataPoints(String pointsString) {
        List<DataPointInterface> points = new ArrayList<>();
        if (pointsString != null && !pointsString.isEmpty()) {
            String[] pointsArray = pointsString.split(";");

            for (String onePoint : pointsArray) {
                String[] keyValue = onePoint.split(":");
                try {
                    if (keyValue.length == 2) {
                        double x = Double.valueOf(keyValue[0]);
                        double y = Double.valueOf(keyValue[1]);
                        DataPoint point = new DataPoint(x, y);
                        points.add(point);
                    }
                } catch (Exception e) {
                    Log.e("ResourceManager", e.getMessage());
                }
            }
        }

        return points.toArray(new DataPointInterface[0]);
    }
}
