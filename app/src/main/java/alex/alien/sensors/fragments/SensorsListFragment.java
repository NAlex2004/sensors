package alex.alien.sensors.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import alex.alien.sensors.R;
import alex.alien.sensors.SensorItem;
import alex.alien.sensors.SensorsListAdapter;

public class SensorsListFragment extends Fragment {
    private ArrayList<SensorItem> _items;
    private static final String ITEMS_KEY = "items";

    public static SensorsListFragment newInstance(ArrayList<SensorItem> items) {
        SensorsListFragment fragment = new SensorsListFragment();
        Bundle bundle = new Bundle();
        if (items == null) {
            items = new ArrayList<>();
        }
        bundle.putSerializable(ITEMS_KEY, items);
        fragment.setArguments(bundle);
        return  fragment;
    }

    public SensorsListFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments != null) {
            try {
                _items = (ArrayList<SensorItem>) arguments.getSerializable(ITEMS_KEY);
                return;
            } catch (Exception e) {}
        }

        if (_items == null) {
            _items = new ArrayList<>();
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sensors_list, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.listView);
        SensorsListAdapter adapter = new SensorsListAdapter(_items);
        recyclerView.setAdapter(adapter);
        return view;
    }
}
