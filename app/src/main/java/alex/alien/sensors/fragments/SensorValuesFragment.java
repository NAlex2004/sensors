package alex.alien.sensors.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import alex.alien.sensors.AutoBrightnessService;
import alex.alien.sensors.R;

public class SensorValuesFragment extends Fragment {

    public static SensorValuesFragment newInstance() {
        return new SensorValuesFragment();
    }

    public SensorValuesFragment() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sensor_values_list, container, false);
        ListView sensorValuesView = view.findViewById(R.id.sensorValuesList);
        Float[] sensorValues = AutoBrightnessService.getObtainedSensorValues();
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, getStringArray(sensorValues));
        sensorValuesView.setAdapter(adapter);
        return view;
    }

    private String[] getStringArray(Float[] source) {
        if (source == null) {
            return new String[0];
        }

        String[] array = new String[source.length];
        int i = 0;
        for (Float element : source) {
            array[i] = element.toString();
            i++;
        }

        return array;
    }
}
