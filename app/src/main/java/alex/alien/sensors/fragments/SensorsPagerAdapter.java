package alex.alien.sensors.fragments;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

import alex.alien.sensors.App;
import alex.alien.sensors.R;
import alex.alien.sensors.SensorItem;
import alex.alien.sensors.Settings;

public class SensorsPagerAdapter extends FragmentPagerAdapter {
    private ArrayList<SensorItem> _items;
    private Settings _settings;

    public SensorsPagerAdapter(FragmentManager fm, ArrayList<SensorItem> sensorItems) {
        this(fm, sensorItems, null);
    }

    public SensorsPagerAdapter(FragmentManager fm, ArrayList<SensorItem> sensorItems, Settings settings) {
        super(fm);
        _items = (sensorItems != null) ? sensorItems : new ArrayList<SensorItem>();
        _settings = settings;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = BrightnessFragment.newInstance();
                break;
            case 1:
                fragment = SensorsListFragment.newInstance(_items);
                break;
            case 2:
                fragment = new SensorValuesFragment();
                break;
            default:
                fragment = BrightnessFragment.newInstance();
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                
                return App.getContext().getString(R.string.brightness);
            case 1:
                return App.getContext().getString(R.string.sensors);
            case 2:
                return App.getContext().getString(R.string.sensorValues);
            default:
                return "Some shit..";
        }
    }
}
