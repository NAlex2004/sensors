package alex.alien.sensors.fragments;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import alex.alien.sensors.MainActivity;
import alex.alien.sensors.R;
import alex.alien.sensors.Settings;
import alex.alien.sensors.databinding.FragmentBrightnessBinding;
import alex.alien.sensors.management.BrightnessEvent;
import alex.alien.sensors.management.BrightnessEventListener;
import alex.alien.sensors.management.LightManagerBase;
import alex.alien.sensors.management.SensorUnavailableException;

public class BrightnessFragment extends Fragment implements BrightnessEventListener {
    private LightManagerBase lightManager;
    private SensorManager sensorManager;
    private Sensor lightSensor;
    private TextView sensorView;
    private TextView brightnessView;
    private TextView percentView;
    private TextView realSensorView;

    @Override
    public void onResume() {
        super.onResume();
        sensorManager.registerListener(lightManager, lightSensor, DELAY);
    }

    private final int DELAY = 500000; // 0.5 sec


    @Override
    public void onPause() {
        super.onPause();
        sensorManager.unregisterListener(lightManager);
    }
    public BrightnessFragment() {

    }

    public static BrightnessFragment newInstance() {
        BrightnessFragment fragment = new BrightnessFragment();

        return  fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            Settings settings = getSettings();
            lightManager = new LightManagerBase(getContext(), this, settings);
        } catch (SensorUnavailableException e) {
            Log.e("LightManager", "No light sensor available");
        }

        sensorManager = (SensorManager) getContext().getSystemService(Context.SENSOR_SERVICE);
        lightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Settings settings = getSettings();

        FragmentBrightnessBinding binding = FragmentBrightnessBinding.inflate(inflater, container, false);
        binding.setSettings(settings);
        View view = binding.getRoot();
        //        View view = inflater.inflate(R.layout.fragment_brightness, container, false);
        sensorView = view.findViewById(R.id.lightValue);
        brightnessView = view.findViewById(R.id.brightnessValue);
        realSensorView = view.findViewById(R.id.realLightValue);
        percentView = view.findViewById(R.id.brightnessPercent);

        return  view;
    }

    @Override
    public void onBrightnessChanges(BrightnessEvent event) {
        if (isVisible() && getUserVisibleHint()) {
            sensorView.setText(String.valueOf(event.currentSensorValue));
            brightnessView.setText(String.valueOf(event.currentBrightness));
            realSensorView.setText(String.valueOf(event.realSensorValue));
            percentView.setText(String.valueOf(event.currentPercent));
        }
    }

    private Settings getSettings() {
        MainActivity activity = (MainActivity)getActivity();
        Settings settings = activity.getAppSettings();
        return settings;
    }
}
