package alex.alien.sensors;

import android.os.Parcel;
import android.os.Parcelable;

public class SensorItem implements Parcelable {

    public String getTitle() {
        return _title;
    }

    public void setTitle(String title) {
        this._title = title;
    }

    public String getDescription() {
        return _description;
    }

    public void setDescription(String description) {
        this._description = description;
    }

    public String getValue() {
        return _value;
    }

    public void setValue(String value) {
        this._value = value;
    }

    private String _title = "";
    private String _description = "";
    private String _value = "";

    public SensorItem() {
    }

    public SensorItem(String title, String description, String value) {
        _title = (title != null) ? title : "";
        _description = (description != null) ? description : "";
        _value = (value != null) ? value : "";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(_title);
        dest.writeString(_description);
        dest.writeString(_value);
    }

    public static final Parcelable.Creator<SensorItem> CREATOR = new Parcelable.Creator<SensorItem>() {
        @Override
        public SensorItem createFromParcel(Parcel source) {
            String title = source.readString();
            String description = source.readString();
            String value = source.readString();

            return new SensorItem(title, description, value);
        }

        @Override
        public SensorItem[] newArray(int size) {
            return new SensorItem[0];
        }
    };
}
