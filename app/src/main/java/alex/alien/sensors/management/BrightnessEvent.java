package alex.alien.sensors.management;

public class BrightnessEvent {
    public int currentBrightness;
    public int previousBrightness;
    public float currentSensorValue;
    public float previousSensorValue;
    public float realSensorValue;
    public int currentPercent;
}
