package alex.alien.sensors.management;

public class SensorUnavailableException extends Exception {
    public SensorUnavailableException(String message) {
        super(message);
    }
}
