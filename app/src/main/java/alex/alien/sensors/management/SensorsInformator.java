package alex.alien.sensors.management;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import alex.alien.sensors.R;
import alex.alien.sensors.SensorItem;

public class SensorsInformator {
    public static ArrayList<SensorItem> getSensors(@NotNull Context context) {
        SensorManager sensorManager = (SensorManager)context.getSystemService(Context.SENSOR_SERVICE);
        List<Sensor> sensors = sensorManager.getSensorList(Sensor.TYPE_ALL);
        ArrayList<SensorItem> sensorItems = new ArrayList<>();
        for (Sensor sensor : sensors) {
            SensorItem item = new SensorItem();
            item.setTitle(sensor.getName());
            item.setDescription(sensor.getStringType());
            item.setValue(String.format(context.getString(R.string.maxRange) + " %s", String.valueOf(sensor.getMaximumRange())));
            sensorItems.add(item);
        }

        return sensorItems;
    }

    public static Sensor getLightSensor(Context context) {
        SensorManager sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        return sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
    }
}
