package alex.alien.sensors.management;

public interface BrightnessEventListener {
    void onBrightnessChanges(BrightnessEvent event);
}
