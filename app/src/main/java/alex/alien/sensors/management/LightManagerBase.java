package alex.alien.sensors.management;

import android.annotation.TargetApi;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;

import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.DataPointInterface;

import java.util.Arrays;
import java.util.Comparator;

import alex.alien.sensors.Settings;

public class LightManagerBase implements SensorEventListener {
    protected float previousSensorValue;
    protected float maximumSensorValue;
    protected float currentSensorValue;
    protected float realSensorValue;
    protected int previousBrightness;
    protected int currentBrightness;
    protected int percent;
    protected Context context;
    protected BrightnessEventListener brightnessEventListener;
    protected Settings settings;

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == Sensor.TYPE_LIGHT) {
            previousSensorValue = currentSensorValue;
            currentSensorValue = sensorEvent.values[0];
            realSensorValue = currentSensorValue;

            int brightness = computeBrightness();
            setBrightness(brightness);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @TargetApi(Build.VERSION_CODES.M)
    protected boolean canWriteSettings() {
        return android.provider.Settings.System.canWrite(context);
    }

    protected int computeBrightness() {
        if (currentSensorValue == 0 && percent > Settings.BOUNDARY_PERCENT) {
            currentSensorValue = maximumSensorValue;
        }

        percent = (int) Math.round(Math.min(getBrightnessPercent(currentSensorValue), 100));
        int level = Settings.MAX_BRIGHTNESS * percent / 100;
        if (currentSensorValue > 0) {
            level = Math.max(40, level);
        }

        return level;
    }

    public LightManagerBase(Context context, BrightnessEventListener brightnessEventListener) throws SensorUnavailableException {
        this(context, brightnessEventListener, null);
    }

    public LightManagerBase(Context context, BrightnessEventListener brightnessEventListener, Settings settings) throws SensorUnavailableException {
        this.brightnessEventListener = brightnessEventListener;
        if (context == null) {
            throw new IllegalArgumentException("Context cannot be null");
        }
        SensorManager sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        Sensor lightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        if (lightSensor == null) {
            throw new SensorUnavailableException("Light sensor is unavailable");
        }

        this.settings = settings != null ? settings : Settings.createInstance(context);
        this.settings.setRealSensorEventDelay(Math.max(this.settings.getRealSensorEventDelay(), 300));

        maximumSensorValue = lightSensor.getMaximumRange();
        currentSensorValue = maximumSensorValue / 2;
        previousSensorValue = currentSensorValue;
        currentBrightness = Math.round(Settings.MAX_BRIGHTNESS / 2);
        previousBrightness = currentBrightness;

        this.context = context;
    }

    public int getBrightness() {
        int result;
        result = android.provider.Settings.System.getInt(context.getContentResolver(), android.provider.Settings.System.SCREEN_BRIGHTNESS, 0);

        return result;
    }

    // override and actually set brightness
    protected void setSystemBrightness(int lastBrightness, int newBrightness) {

    }

    protected void brightnessChanged() {
        if (brightnessEventListener != null) {
            BrightnessEvent event = new BrightnessEvent();
            event.currentBrightness = currentBrightness;
            event.previousBrightness = previousBrightness;
            event.currentSensorValue = currentSensorValue;
            event.previousSensorValue = previousSensorValue;
            event.realSensorValue = realSensorValue;
            event.currentPercent = percent;

            brightnessEventListener.onBrightnessChanges(event);
        }
    }

    public void setBrightness(int value) {
        int actualBrightness = previousBrightness;
        currentBrightness = Math.max(Settings.MIN_BRIGHTNESS, value);
        setSystemBrightness(actualBrightness, currentBrightness);
        brightnessChanged();
    }

    protected double getBrightnessPercent(float sensorValue) {
        DataPointInterface[] points = settings.getDataPoints();
        int index = Arrays.binarySearch(points, new DataPoint(sensorValue, 0), new Comparator<DataPointInterface>() {
            @Override
            public int compare(DataPointInterface p1, DataPointInterface p2) {
                return Double.compare(p1.getX(), p2.getX());
            }
        });

        if (index < 0) {
            index = -index - 1;
            // All sensor values are less than given
            if (index == points.length) {
                return 100;
            }
            if (index != 0) {
                double diff1 = Math.abs(points[index].getX() - sensorValue);
                double diff2 = Math.abs(sensorValue - points[index - 1].getX());

                index = diff1 <= diff2
                        ? index
                        : index - 1;
            }
        }

        return points[index].getY();
    }
}
