package alex.alien.sensors.management;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;

import alex.alien.sensors.Settings;

public class LightManager extends LightManagerBase {
    private Handler messageHandler;
    private boolean isMessageScheduled = false;

    public LightManager(Context context, BrightnessEventListener brightnessEventListener, int delay) throws SensorUnavailableException {
        this(context, brightnessEventListener, null);
        settings.setRealSensorEventDelay(Math.max(delay, 300));
        messageHandler = new Handler();
    }

    public LightManager(Context context, BrightnessEventListener brightnessEventListener, Settings settings) throws SensorUnavailableException {
        super(context, brightnessEventListener, settings);
        messageHandler = new Handler();
    }

    @Override
    protected void setSystemBrightness(final int lastBrightness, final int newBrightness) {
        if (isMessageScheduled) {
            return;
        }

        boolean canSetBrightness = (Build.VERSION.SDK_INT < Build.VERSION_CODES.M || canWriteSettings());

        if (canSetBrightness) {
            isMessageScheduled = true;
            int delay = settings.getRealBrightnessChangeDelay();

            messageHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    isMessageScheduled = false;

                    int delta = Math.abs(currentBrightness - previousBrightness);
                    int deltaBoundary = (currentBrightness < 125)
                            ? settings.getRealBrightnessDeltaLow()
                            : settings.getRealBrightnessDeltaHigh();
                    if (delta <  deltaBoundary) {
                        return;
                    }
                    // params are not used, because current brightness may change during delay
                    int actualBrightness = previousBrightness;
                    previousBrightness = currentBrightness;
                    final ContentResolver contentResolver = context.getContentResolver();

                    BrightnessSetterThread brightnessSetter = new BrightnessSetterThread(actualBrightness, currentBrightness, contentResolver);
                    brightnessSetter.run();
                }
            }, delay);


        } else {
            Intent intent = new Intent(android.provider.Settings.ACTION_MANAGE_WRITE_SETTINGS)
                    .setData(Uri.parse("package:" + context.getPackageName()))
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }


    private class BrightnessSetterThread implements Runnable {
        private final int oldBrightnessValue;
        private final int brightnessValue;
        private final ContentResolver contentResolver;


        public BrightnessSetterThread(int oldBrightnessValue, int newBrightnessValue, ContentResolver contentResolver) {
            this.oldBrightnessValue = oldBrightnessValue;
            this.brightnessValue = newBrightnessValue;
            this.contentResolver = contentResolver;
        }

        private void setBrightness(int value) {
            android.provider.Settings.System.putInt(contentResolver, android.provider.Settings.System.SCREEN_BRIGHTNESS_MODE, android.provider.Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL);
            android.provider.Settings.System.putInt(contentResolver, android.provider.Settings.System.SCREEN_BRIGHTNESS, value);
        }

        @Override
        public void run() {
            int step = settings.getRealBrightnessSteps();
            int newValue = oldBrightnessValue;
            int brightnessStepDelta = (brightnessValue - oldBrightnessValue) / settings.getRealBrightnessSteps();

            boolean needSteps = (brightnessValue >= (Settings.MAX_BRIGHTNESS / 2))
                    ? Math.abs(brightnessStepDelta) >= settings.getRealMinStepDeltaHighBrightness()
                    : Math.abs(brightnessStepDelta) >= settings.getRealMinStepDeltaLowBrightness();
            if (needSteps) {
                while (step > 1) {
                    newValue += brightnessStepDelta;
                    setBrightness(newValue);

                    try {
                        Thread.sleep(settings.getRealStepDuration());
                    } catch (InterruptedException e) {
                        break;
                    }

                    step--;
                }
            }

            setBrightness(brightnessValue);
        }
    }

}
