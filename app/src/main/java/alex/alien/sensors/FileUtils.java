package alex.alien.sensors;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class FileUtils {
    public static SortedSet<Float> LoadSensorValues(String fileName, Context context) {
        SortedSet<Float> values = new TreeSet<>();

        File file = new File(context.getFilesDir(), fileName);
        if (file.exists()) {
            try (FileInputStream inputStream = context.openFileInput(fileName)) {
                InputStreamReader inputReader = new InputStreamReader(inputStream);
                BufferedReader reader = new BufferedReader(inputReader);

                while (reader.ready()) {
                    String valueString = reader.readLine();
                    Float value = Float.valueOf(valueString);
                    values.add(value);
                }
                reader.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Log.e("FileUtils", "File with sensor values not found");
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("FileUtils", e.getMessage());
            }
        }

        return values;
    }

    public static void SaveSensorValues(Set<Float> values, String filename, Context context) {
        try (FileOutputStream outputStream = context.openFileOutput(filename, Context.MODE_PRIVATE)) {
            OutputStreamWriter streamWriter = new OutputStreamWriter(outputStream);
            BufferedWriter writer = new BufferedWriter(streamWriter);
            for (Float value : values) {
                writer.write(String.valueOf(value));
                writer.newLine();
            }
            writer.flush();
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Log.e("FileUtils", "File with sensor values not found");
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("FileUtils", e.getMessage());
        }
    }
}
