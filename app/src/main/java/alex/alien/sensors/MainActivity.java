package alex.alien.sensors;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.jjoe64.graphview.series.DataPointInterface;

import alex.alien.sensors.chart.GraphActivity;
import alex.alien.sensors.fragments.SensorsPagerAdapter;
import alex.alien.sensors.management.SensorsInformator;

public class MainActivity extends AppCompatActivity {
    private SettingsManager settingsManager;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    private final static int REQUEST_CODE = 1;

    public Settings getAppSettings() {
        initSettingsManager();
        return settingsManager.getSettings();
    }

    private void initSettingsManager() {
        if (settingsManager == null) {
            settingsManager = new SettingsManager(this);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initSettingsManager();
        ViewPager pager = findViewById(R.id.pager);
        SensorsPagerAdapter adapter = new SensorsPagerAdapter(getSupportFragmentManager(), SensorsInformator.getSensors(this), settingsManager.getSettings());
        pager.setAdapter(adapter);
    }

    public void startServiceClick(View view) {
        Intent intent = new Intent(this, AutoBrightnessService.class);
        intent.putExtra(Settings.class.getCanonicalName(), settingsManager.getSettings());
        startService(intent);
    }

    public void stopServiceClick(View view) {
        Intent intent = new Intent(this, AutoBrightnessService.class);
        stopService(intent);
    }

    public void saveSettingsClick(View view) {
        settingsManager.writeSettings();
    }

    public void resetSettingsClick(View view) {
        settingsManager.resetToDefault();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode != REQUEST_CODE) {
            return;
        }

        if (resultCode == RESULT_OK && data != null) {
            try {
                DataPointInterface[] points = (DataPointInterface[])data.getSerializableExtra(GraphActivity.DATA_KEY);
                if (points != null) {
                    settingsManager.getSettings().setDataPoints(points);
                    settingsManager.writeSettings();

                    if (AutoBrightnessService.isStarted()) {
                        restartService();
                    }

                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);
//                    restartService();
                }
            } catch (Exception e) {
                Log.e("onActivityResult", e.getMessage());
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            }

        }
    }

    public void brightnessChartClick(MenuItem item) {
        Intent graphIntent = new Intent(this, GraphActivity.class);
        DataPointInterface[] points = settingsManager.getSettings().getDataPoints();
        double maxX = points[points.length - 1].getX();
        graphIntent.putExtra(GraphActivity.MAX_X_KEY, maxX);
        graphIntent.putExtra(GraphActivity.MAX_Y_KEY, 100d);
        graphIntent.putExtra(GraphActivity.DATA_KEY, settingsManager.getSettings().getDataPoints());
        startActivityForResult(graphIntent, REQUEST_CODE);
    }



    protected void restartService() {
        stopServiceClick(null);
        startServiceClick(null);
    }
}
