package alex.alien.sensors;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class AutoBrightnessBootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent serviceIntent = new Intent(context, AutoBrightnessService.class);
        SettingsManager settingsManager = new SettingsManager(context);
        serviceIntent.putExtra(Settings.class.getCanonicalName(), settingsManager.getSettings());
        context.startService(serviceIntent);
    }
}
