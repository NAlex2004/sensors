package alex.alien.sensors;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import java.util.SortedSet;

import alex.alien.sensors.management.LightManager;
import alex.alien.sensors.management.SensorUnavailableException;

import static android.support.v4.app.NotificationCompat.PRIORITY_MIN;

public class AutoBrightnessService extends Service {
    private static final int ID_SERVICE = 65432;
    private static final String OBTAINED_VALUES_FILENAME = "sensor_values";

    private Settings settings;
    private static boolean started = false;
    private LightManager lightManager;
    private static SortedSet<Float> obtainedSensorValues = null;
    private LightSensorEventListener sensorValuesEventListener = new LightSensorEventListener();

    public AutoBrightnessService() {
    }

    public static boolean isStarted() {
        return started;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Intent mainActivityIntent = new Intent(this, MainActivity.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntentWithParentStack(mainActivityIntent);
        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String channelId = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O ? createNotificationChannel(notificationManager) : "";
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId);
        Notification notification = notificationBuilder.setOngoing(true)
                .setContentTitle(getString(R.string.title))
                .setSmallIcon(R.drawable.ic_automatic_brightness)
                .setPriority(PRIORITY_MIN)
                .setCategory(NotificationCompat.CATEGORY_SERVICE)
                .setContentIntent(pendingIntent)
                .build();

        startForeground(ID_SERVICE, notification);
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private String createNotificationChannel(NotificationManager notificationManager){
        String channelId = "AutoBrightnessServiceChannel_ID_65432";
        String channelName = "AutoBrightnessServiceChannel";
        NotificationChannel channel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_HIGH);
        // omitted the LED color
        channel.setImportance(NotificationManager.IMPORTANCE_NONE);
        channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        notificationManager.createNotificationChannel(channel);
        return channelId;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        obtainedSensorValues = FileUtils.LoadSensorValues(OBTAINED_VALUES_FILENAME, this);
        settings = intent.getParcelableExtra(Settings.class.getCanonicalName());
        if (settings == null) {
            settings = Settings.createInstance(this);
        }

        if (!started) {
            SensorManager sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
            Sensor lightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
            try {
                lightManager = new LightManager(this, null, settings);
                sensorManager.registerListener(lightManager, lightSensor, settings.getRealSensorEventDelay());
                if (settings.getCollectSensorValues()) {
                    sensorManager.registerListener(sensorValuesEventListener, lightSensor, SensorManager.SENSOR_DELAY_NORMAL);
                }
            } catch (SensorUnavailableException e) {
                Log.e("LightManager", "No light sensor available");
                started = false;
                Toast.makeText(this, getString(R.string.noSensor), Toast.LENGTH_LONG).show();
                stopSelf();
            }
            started = true;
        }

        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        SensorManager sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensorManager.unregisterListener(lightManager);
        if (settings.getCollectSensorValues()) {
            sensorManager.unregisterListener(sensorValuesEventListener);
            FileUtils.SaveSensorValues(obtainedSensorValues, OBTAINED_VALUES_FILENAME, this);
        }

        started = false;
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return null;
    }

    public static Float[] getObtainedSensorValues() {
        if (obtainedSensorValues == null) {
            return new Float[0];
        }

        return obtainedSensorValues.toArray(new Float[0]);
    }

    // SensorEventListener
    // collect light sensor values.
    public class LightSensorEventListener implements SensorEventListener {
        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {
            if (sensorEvent.sensor.getType() == Sensor.TYPE_LIGHT) {
                obtainedSensorValues.add(sensorEvent.values[0]);
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int i) {

        }
    }
}
