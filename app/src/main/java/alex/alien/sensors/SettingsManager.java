package alex.alien.sensors;

import android.content.Context;
import android.content.SharedPreferences;

import com.jjoe64.graphview.series.DataPointInterface;

public class SettingsManager {
    private final static String BOUNDARY_PERCENT = "BOUNDARY_PERCENT";
    private final static String BRIGHTNESS_CHANGE_DELAY = "BRIGHTNESS_CHANGE_DELAY";
    private final static String STEPS_COUNT = "STEPS_COUNT";
    private final static String STEP_DURATION = "STEP_DURATION";
    private final static String MIN_STEP_DELTA_HIGHT_BRIGHTNESS = "MIN_STEP_DELTA_HIGH_BRIGHTNESS";
    private final static String MIN_STEP_DELTA_LOW_BRIGHTNESS = "MIN_STEP_DELTA_LOW_BRIGHTNESS";
    private final static String BRIGHTNESS_DELTA_LOW = "BRIGHTNESS_DELTA_LOW";
    private final static String BRIGHTNESS_DELTA_HIGH = "BRIGHTNESS_DELTA_HIGH";
    private final static String DATAPOINTS = "DATAPOINTS";


    private final Context context;
    private final Settings _settings;
    private final SharedPreferences preferences;

    public SettingsManager(Context context) {
        this.context = context;
        this.preferences = context.getSharedPreferences(Settings.PREFERENCES_NAME, Context.MODE_PRIVATE);
        _settings = Settings.createInstance(context);
        readSettings();
    }

    public Settings getSettings() {
        return _settings;
    }

    public void resetToDefault() {
        _settings.setRealBoundaryPercent(Settings.BOUNDARY_PERCENT);
        _settings.setRealBrightnessChangeDelay(Settings.BRIGHTNESS_CHANGE_DELAY);
        _settings.setRealBrightnessSteps(Settings.STEPS_COUNT);
        _settings.setRealStepDuration(Settings.STEP_DURATION);
        _settings.setRealMinStepDeltaHighBrightness(Settings.MIN_STEP_DELTA_HIGH_BRIGHTNESS);
        _settings.setRealMinStepDeltaLowBrightness(Settings.MIN_STEP_DELTA_LOW_BRIGHTNESS);
        _settings.setRealBrightnessDeltaLow(Settings.BRIGHTNESS_DELTA_LOW);
        _settings.setRealBrightnessDeltaHigh(Settings.BRIGHTNESS_DELTA_HIGH);
        _settings.setDataPoints(Settings.getDefaultBrightnessPoints(context));
    }

    public void readSettings() {
        _settings.setRealBoundaryPercent(preferences.getInt(BOUNDARY_PERCENT, Settings.BOUNDARY_PERCENT));
        _settings.setRealBrightnessChangeDelay(preferences.getInt(BRIGHTNESS_CHANGE_DELAY, Settings.BRIGHTNESS_CHANGE_DELAY));
        _settings.setRealBrightnessSteps(preferences.getInt(STEPS_COUNT, Settings.STEPS_COUNT));
        _settings.setRealStepDuration(preferences.getInt(STEP_DURATION, Settings.STEP_DURATION));
        _settings.setRealMinStepDeltaHighBrightness(preferences.getInt(MIN_STEP_DELTA_HIGHT_BRIGHTNESS, Settings.MIN_STEP_DELTA_HIGH_BRIGHTNESS));
        _settings.setRealMinStepDeltaLowBrightness(preferences.getInt(MIN_STEP_DELTA_LOW_BRIGHTNESS, Settings.MIN_STEP_DELTA_LOW_BRIGHTNESS));
        _settings.setRealBrightnessDeltaLow(preferences.getInt(BRIGHTNESS_DELTA_LOW, Settings.BRIGHTNESS_DELTA_LOW));
        _settings.setRealBrightnessDeltaHigh(preferences.getInt(BRIGHTNESS_DELTA_HIGH, Settings.BRIGHTNESS_DELTA_HIGH));
        String dataPointsStr = preferences.getString(DATAPOINTS, "");
        DataPointInterface[] points = ResourceManager.stringToDataPoints(dataPointsStr);
        if (points.length == 0) {
            points = Settings.getDefaultBrightnessPoints(context);
        }
        _settings.setDataPoints(points);
    }

    public void writeSettings() {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(BOUNDARY_PERCENT, _settings.getRealBoundaryPercent());
        editor.putInt(BRIGHTNESS_CHANGE_DELAY, _settings.getRealBrightnessChangeDelay());
        editor.putInt(STEPS_COUNT, _settings.getRealBrightnessSteps());
        editor.putInt(STEP_DURATION, _settings.getRealStepDuration());
        editor.putInt(MIN_STEP_DELTA_HIGHT_BRIGHTNESS, _settings.getRealMinStepDeltaHighBrightness());
        editor.putInt(MIN_STEP_DELTA_LOW_BRIGHTNESS, _settings.getRealMinStepDeltaLowBrightness());
        editor.putInt(BRIGHTNESS_DELTA_LOW, _settings.getRealBrightnessDeltaLow());
        editor.putInt(BRIGHTNESS_DELTA_HIGH, _settings.getRealBrightnessDeltaHigh());
        String pointsStr = ResourceManager.dataPointsToString(_settings.getDataPoints());
        editor.putString(DATAPOINTS, pointsStr);
        editor.commit();
    }

}
