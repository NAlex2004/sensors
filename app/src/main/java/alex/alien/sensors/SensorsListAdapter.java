package alex.alien.sensors;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class SensorsListAdapter extends RecyclerView.Adapter<SensorsListAdapter.SensorItemViewHolder> {
    private ArrayList<SensorItem> _items;

    public SensorsListAdapter(ArrayList<SensorItem> items) {
        _items = (items != null) ? items : new ArrayList<SensorItem>();
    }

    @NonNull
    @Override
    public SensorItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        Context context = viewGroup.getContext();
        View itemView = LayoutInflater.from(context).inflate(R.layout.sensor_item, viewGroup, false);

        return new SensorItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SensorItemViewHolder sensorItemViewHolder, int position) {
        SensorItem item = _items.get(position);
        sensorItemViewHolder.Title.setText(item.getTitle());
        sensorItemViewHolder.Description.setText(item.getDescription());
        sensorItemViewHolder.Value.setText(item.getValue());
    }

    @Override
    public int getItemCount() {
        return _items.size();
    }

    public class SensorItemViewHolder extends RecyclerView.ViewHolder {
        final TextView Title;
        final TextView Description;
        final TextView Value;

        public SensorItemViewHolder(@NonNull View itemView) {
            super(itemView);

            Title = itemView.findViewById(R.id.sensorTitle);
            Description = itemView.findViewById(R.id.sensorDescription);
            Value = itemView.findViewById(R.id.sensorValue);
        }
    }
}
