package alex.alien.sensors.chart;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.DataPointInterface;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.SortedSet;
import java.util.TreeSet;

public class GraphDataSupplier {
    private static final int POINT_RADIUS = 10;

    private SortedSet<DataPointInterface> points = new TreeSet<>(new Comparator<DataPointInterface>() {
        @Override
        public int compare(DataPointInterface o1, DataPointInterface o2) {
            return Double.compare(o1.getX(), o2.getX());
        }
    });
    private LineGraphSeriesAlien<DataPointInterface> dataSeries;
    protected double highestValueX = 0;
    protected double highestValueY = 0;

    private void initPoints(DataPointInterface[] dataPointsArray) {
        points.clear();
        if (dataPointsArray != null) {
            for (DataPointInterface point : dataPointsArray) {
                if (isPointInBounds(point)) {
                    points.add(point);
                }
            }
        }
    }

    private void initDataSeries(DataPointInterface[] dataPoints) {
        dataSeries = new LineGraphSeriesAlien<>(points.toArray(new DataPointInterface[0]));
        dataSeries.setDrawDataPoints(true);
        dataSeries.setDataPointsRadius(POINT_RADIUS);
    }

    /**
     * Highest X/Y values are taken from dataPoints
     * @param dataPoints
     */
    public GraphDataSupplier(DataPointInterface[] dataPoints) {
        initHighestValues(dataPoints);
        // initHighestValues first, don't include points out of the bounds
        initPoints(dataPoints);
        initDataSeries(dataPoints);
    }

    /**
     * Provide highest X/Y values as params
     * Points that are out of bounds are not included to data source
     * @param dataPoints
     * @param highestValueX
     * @param highestValueY
     */
    public GraphDataSupplier(DataPointInterface[] dataPoints, double highestValueX, double highestValueY) {
        this.highestValueX = highestValueX;
        this.highestValueY = highestValueY;
        // initHighestValues first, don't include points out of the bounds
        initPoints(dataPoints);
        initDataSeries(dataPoints);
    }

    public LineGraphSeriesAlien<DataPointInterface> getDataSeries() {
        return dataSeries;
    }

    protected void initHighestValues(DataPointInterface[] data) {
        if (data != null && data.length > 0) {
            // array is x-sorted asc
            highestValueX = data[data.length - 1].getX();
            highestValueY = Collections.max(Arrays.asList(data), new Comparator<DataPointInterface>() {
                @Override
                public int compare(DataPointInterface e1, DataPointInterface e2) {
                    return Double.compare(e1.getY(), e2.getY());
                }
            }).getY();
        }
    }

    public void addPoint(DataPointInterface point) {
        points.add(point);
        dataSeries.resetData(points.toArray(new DataPointInterface[0]));
    }

    /**
     * Moves oldPoint to new position (replace oldPoint be newPoint) and calls resetData on data set to refresh view.
     * If newPoint or oldPoint == null - does nothing
     * @param oldPoint
     * @param newPoint
     */
    public void replacePoint(DataPointInterface oldPoint, DataPointInterface newPoint) {
        if (newPoint == null || oldPoint == null) {
            return;
        }
        points.remove(oldPoint);
        points.add(newPoint);
        dataSeries.resetData(points.toArray(new DataPointInterface[0]));
    }

    public DataPointInterface findDataPoint(float x, float y) {
        return dataSeries.findDataPointByXY(x, y);
    }

    /**
     * Converts screen (view) coordinates to data values (DataPoint)
     * @param view
     * @param x
     * @param y
     * @return null if point is out of 0..max(X/Y) of existing values
     */
    public DataPointInterface convertViewCoordsToDataPoint(GraphView view, float x, float y) {
        float viewX = x - view.getGraphContentLeft();
        float viewY = view.getGraphContentHeight() + view.getGraphContentTop() - y;

        double deltaY = view.getGraphContentHeight() / highestValueY;
        double deltaX = view.getGraphContentWidth() / highestValueX;

        viewX /= deltaX;
        viewY /= deltaY;

        if (viewX < 0 || viewX > highestValueX || viewY < 0 || viewY > highestValueY) {
            return null;
        }

        return new DataPoint(viewX, viewY);
    }

    protected boolean isPointInBounds(DataPointInterface point) {
        return point.getX() >= 0 && point.getX() <= highestValueX
                && point.getY() >= 0 && point.getY() <= highestValueY;
    }
}
