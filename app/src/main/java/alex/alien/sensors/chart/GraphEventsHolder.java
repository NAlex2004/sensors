package alex.alien.sensors.chart;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPointInterface;

public class GraphEventsHolder implements View.OnTouchListener, View.OnLongClickListener, View.OnDragListener {
    protected GraphDataSupplier mDataSupplier;
    private PointF mLastTouchPoint = new PointF(-100, -100);
//    private PointF mDraggedPoint = null;
    private DataPointInterface mDraggedDataPoint = null;

    public GraphEventsHolder(GraphDataSupplier dataSupplier) {
        mDataSupplier = dataSupplier;
    }

    @Override
    public boolean onDrag(View v, DragEvent event) {
        final int action = event.getAction();
        switch (action) {
            case DragEvent.ACTION_DRAG_STARTED:
                return true;
            case DragEvent.ACTION_DRAG_ENTERED:
                return true;
            case DragEvent.ACTION_DRAG_LOCATION:
                return true;
            case DragEvent.ACTION_DRAG_ENDED:
                mDraggedDataPoint = null;
//                mDraggedPoint = null;
                return true;
            case DragEvent.ACTION_DRAG_EXITED:
                mDraggedDataPoint = null;
                return true;
            case DragEvent.ACTION_DROP:
                if (mDraggedDataPoint != null) {
                    DataPointInterface newPoint = mDataSupplier.convertViewCoordsToDataPoint((GraphView) v, event.getX(), event.getY());
                    mDataSupplier.replacePoint(mDraggedDataPoint, newPoint);
                }
                return true;
        }

        return false;
    }

    @Override
    public boolean onLongClick(View v) {
        DataPointInterface dataPoint = mDataSupplier.findDataPoint(mLastTouchPoint.x, mLastTouchPoint.y);
        if (dataPoint != null) {
//            mDraggedPoint = new PointF(mLastTouchPoint.x, mLastTouchPoint.y);
            mDraggedDataPoint = dataPoint;
            v.startDrag(null, new CircleDragShadowBuilder(v), null, 0);
            return true;
        }
        return false;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
            mLastTouchPoint.set(event.getX(), event.getY());
        }
        return false;
    }

    //------------------------ CircleDragShadowBuilder ---------------------------------
    class CircleDragShadowBuilder extends View.DragShadowBuilder {
        private int radius = 20;

        public CircleDragShadowBuilder() {
            super();
            radius = Math.round(mDataSupplier.getDataSeries().getDataPointsRadius() * 2);
        }

        public CircleDragShadowBuilder(View view) {
            super(view);
            radius = Math.round(mDataSupplier.getDataSeries().getDataPointsRadius());
        }

        @Override
        public void onProvideShadowMetrics(Point outShadowSize, Point outShadowTouchPoint) {
            outShadowSize.set(radius * 2, radius * 2);
            outShadowTouchPoint.set(radius, radius);
        }

        @Override
        public void onDrawShadow(Canvas canvas) {
            Paint paint = new Paint();
            paint.setColor(Color.RED);
            paint.setStyle(Paint.Style.FILL);
            canvas.drawCircle(radius, radius, radius, paint);
        }
    }
    //------------------------------------------------------------------------------------
}
