package alex.alien.sensors.chart;

import android.graphics.PointF;

import com.jjoe64.graphview.series.DataPointInterface;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.jjoe64.graphview.series.OnDataPointTapListener;
import com.jjoe64.graphview.series.Series;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;

public class LineGraphSeriesAlien<E extends DataPointInterface> extends LineGraphSeries<E> {


    public LineGraphSeriesAlien(E[] data) {
        super(data);
        setOnDataPointTapListener(new OnDataPointTapListener() {
            @Override
            public void onTap(Series series, DataPointInterface dataPoint) {
                // Stub, without it no computing screen coords for data points performed!
            }
        });
    }

    public E findDataPointByXY(float x, float y) {

        return findDataPoint(x, y);
    }

    public List<E> getData() {
        Iterator<E> iterator = getValues(0, Double.MAX_VALUE);
        List<E> data = new ArrayList<>();
        while (iterator.hasNext()) {
            data.add(iterator.next());
        }

        return data;
    }
}
