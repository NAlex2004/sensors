package alex.alien.sensors.chart;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.Sensor;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.Viewport;
import com.jjoe64.graphview.series.BaseSeries;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.DataPointInterface;
import com.jjoe64.graphview.series.OnDataPointTapListener;
import com.jjoe64.graphview.series.Series;

import alex.alien.sensors.R;
import alex.alien.sensors.Settings;
import alex.alien.sensors.management.SensorsInformator;

/**
 * Pass to activity in intent maxX, maxY through MAX_X_KEY, MAX_Y_KEY and DataPointInterface[] through DATA_KEY
 *
 * Get result through DATA_KEY in onActivityResult of calling code
 */
public class GraphActivity extends AppCompatActivity implements OnDataPointTapListener {
    private GraphDataSupplier dataSupplier;
    private DataPoint newDataPoint = null;
    private DataPointInterface[] dataPoints;
    public static final String DATA_KEY = "DATA_KEY";
    public static final String MAX_X_KEY = "MAX_X_KEY";
    public static final String MAX_Y_KEY = "MAX_Y_KEY";
    private double maxX = 10240;
    private double maxY = 100;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(DATA_KEY, dataSupplier.getDataSeries().getData().toArray(new DataPointInterface[0]));
        outState.putDouble(MAX_X_KEY, maxX);
        outState.putDouble(MAX_Y_KEY, maxY);
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.settings_menu, menu);
        return true;
    }

    protected void initGraph() {
        dataSupplier = new GraphDataSupplier(dataPoints, maxX, maxY);
        GraphEventsHolder eventsHolder = new GraphEventsHolder(dataSupplier);
        GraphView graphView = findViewById(R.id.graph);
        final BaseSeries<DataPointInterface> series = dataSupplier.getDataSeries();
        Viewport viewPort = graphView.getViewport();
        viewPort.setXAxisBoundsManual(true);
        viewPort.setMaxX(maxX);
        viewPort.setMinX(0);
        viewPort.setYAxisBoundsManual(true);
        viewPort.setMinY(0);
        viewPort.setMaxY(maxY);

        graphView.setTitle(getString(R.string.graphTitle));
        graphView.addSeries(series);
        graphView.setOnTouchListener(eventsHolder);
        graphView.setOnLongClickListener(eventsHolder);
        graphView.setOnDragListener(eventsHolder);
        series.setOnDataPointTapListener(this);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getIntent().getExtras();
        if (arguments != null) {
            maxX = arguments.getDouble(MAX_X_KEY, 0);
            maxY = arguments.getDouble(MAX_Y_KEY, 0);
            dataPoints = (DataPointInterface[])arguments.getSerializable(DATA_KEY);
        }
        setContentView(R.layout.activity_graph);
        if (savedInstanceState == null) {
            if (dataPoints == null) {
                dataPoints = new DataPoint[0];
            }
            initGraph();
        };
        Toast toast = Toast.makeText((Context) this, getString(R.string.dragPoints), Toast.LENGTH_LONG);
        toast.show();
    }

    @Override
    public void onBackPressed() {
        final Context context = this;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle(R.string.apply)
                .setMessage(getString(R.string.apply) + "?")
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent();
                        DataPointInterface[] points = dataSupplier.getDataSeries().getData().toArray(new DataPointInterface[0]);
                        intent.putExtra(DATA_KEY, points);
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                })
                .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        setResult(RESULT_CANCELED);
                        finish();
                    }
                });
        AlertDialog dialog = dialogBuilder.create();
        dialog.show();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        dataPoints = (DataPointInterface[])savedInstanceState.getSerializable(DATA_KEY);
        maxX = savedInstanceState.getDouble(MAX_X_KEY);
        maxY = savedInstanceState.getDouble(MAX_Y_KEY);
        initGraph();
    }

    @Override
    public void onTap(Series series, DataPointInterface dataPoint) {
        Toast toast = Toast.makeText((Context) this, String.format("%.2f; %.2f%%", dataPoint.getX(), dataPoint.getY()), Toast.LENGTH_LONG);
        toast.show();
    }

    public void resetValuesToA465Click(MenuItem item) {
        final Context context = this;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle(getString(R.string.resetValues))
                .setMessage(getString(R.string.resetToA465))
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        DataPointInterface[] points = Settings.getDefaultBrightnessPoints(context);
                        dataSupplier.getDataSeries().resetData(points);
                    }
                })
        .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        AlertDialog dialog = dialogBuilder.create();
        dialog.show();
    }

    public void resetValuesToLineClick(MenuItem item) {
        final Context context = this;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle(getString(R.string.resetValues))
                .setMessage(getString(R.string.resetToLinear))
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Sensor lightSensor = SensorsInformator.getLightSensor(context);
                        if (lightSensor != null) {
                            DataPointInterface[] points = Settings.getLinearDataPoints(lightSensor.getMaximumRange());
                            dataSupplier.getDataSeries().resetData(points);
                        }
                    }
                })
                .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
        AlertDialog dialog = dialogBuilder.create();
        dialog.show();
    }
}
